import abc
import numpy as np

class Trainer(object):
    
    """ Trainer for Feedforward Neural Network """
    
    # Class Members
    fnn = None
    maxIters, maxBestIters, erorTol = np.Infinity, 50, 1e-5
    iter, min_err, bestIter = 0, 1e10, 0
    lamb = 1e-3
    best_params = []
    
    # Class Methods
    def __init__(self, fnn, lamb=1e-5, options=None):
        self.fnn = fnn
        self.lamb = lamb
        if(options is not None):
            self.maxIters, self.maxBestIters, self.erorTol = options
    
    def cost(self, X, y):
        err = 0
        for l in range(self.fnn.layerCount):
            err += (self.lamb/2)*(np.sum(self.fnn.layers[l].params**2))
        y_p = self.fnn.feedforward(X)
        err += np.double(np.sum((y-y_p)**2))*0.5/X.shape[0]
        return err
    
    def numericGrad(self, X, y, l):
        self.fnn.backpropagate(X, y, self.lamb)
        epsilon = 1e-5
        grad = np.zeros(self.fnn.layers[l].grad.shape)
        for i in range(grad.shape[0]):
            for j in range(grad.shape[1]):
                cost_ori = self.cost(X, y)
                self.fnn.layers[l].params[i, j] += epsilon
                cost_plus = self.cost(X, y)
                self.fnn.layers[l].params[i, j] -= epsilon
                grad[i, j] = (cost_plus-cost_ori)/(epsilon)
        return grad

    def train(self, X_train, y_train, X_valid=None, y_valid=None, batchSize=20):
        self.best_params = [self.fnn.layers[l].params.copy() for l in range(self.fnn.layerCount)]
        self.iter, self.min_err, self.bestIter = 0, 1e10, 0
        N = X_train.shape[0]
        self.maxBestIters = min(N*50/batchSize, self.maxBestIters)
        if(X_valid is None):
            X_valid, y_valid = X_train.copy(), y_train.copy()
        n = int(N/batchSize)+1
        while(True):
            randpermu = np.random.permutation(N)
            for i in range(int(n/2)+1):
                idx_j = randpermu[i*batchSize:min((i+1)*batchSize, N-1)]
                X_j, y_j = X_train[idx_j].copy(), y_train[idx_j].copy()
                self.fnn.backpropagate(X_j, y_j, self.lamb)
                grad = []
                for l in range(self.fnn.layerCount):
                    grad.append(self.fnn.layers[l].grad.copy())
                for l in range(self.fnn.layerCount):
                    params = self.fnn.layers[l].params.copy()
                    self.fnn.layers[l].params = self.apply_update_rule(params, grad[l], l).copy()
                self.iter += 1
                err = self.cost(X_valid, y_valid)
                self.err_diff = abs(err-self.min_err)
                if(err < self.min_err):
                    if(self.min_err - err < self.erorTol):
                        self.bestIter += 1
                    else:
                        self.bestIter = 0
                    self.min_err = err
                    self.best_params = [self.fnn.layers[l].params.copy() for l in range(self.fnn.layerCount)]
                else:
                    self.bestIter += 1
                print(self.iter, ":", self.min_err)
                if(self.stop_condition()):
                    break
            if(self.stop_condition()):
                for l in range(self.fnn.layerCount):
                    self.fnn.layers[l].params = self.best_params[l]
                break
    
    def stop_condition(self):
        if(self.iter==self.maxIters or self.bestIter == self.maxBestIters):
            return True
        return False
    
    __metaclass__ = abc.ABCMeta
    
    @abc.abstractmethod
    def apply_update_rule(self, params, grad, l):
        pass
            
            