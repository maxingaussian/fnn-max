import numpy as np
from .Trainer import Trainer as Trainer

class SGD(Trainer):
    
    """ Stochastic Gradient Decent Trainer """
    
    learningRate, momentum = 1e-3, 0.9
    paramsDelta = None
    
    def __init__(self, fnn, lamb=1e-3, options=None, learningRate=1e-3, momentum=0.9):
        self.learningRate = learningRate
        self.momentum = momentum
        self.paramsDelta = [[] for _ in range(fnn.layerCount)]
        super(SGD, self).__init__(fnn, lamb, options)
    
    def apply_update_rule(self, params, grad, l):
        if(len(self.paramsDelta[l])==0):
            self.paramsDelta[l] = np.zeros(params.shape)
        self.paramsDelta[l] = self.learningRate*grad
        self.paramsDelta[l] += self.momentum*self.paramsDelta[l]
        params -= self.paramsDelta[l]
        return params