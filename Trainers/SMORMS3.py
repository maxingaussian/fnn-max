import numpy as np
from .Trainer import Trainer as Trainer

class SMORMS3(Trainer):
    
    """ SMORMS3 Trainer """
    
    mem, g, g2 = [], [], []
    learningRate = 0.005
    
    def __init__(self, fnn, lamb=1e-3, options=None, learningRate=0.005):
        self.learningRate = learningRate
        self.mem = [None for _ in range(fnn.layerCount)]
        self.g = [None for _ in range(fnn.layerCount)]
        self.g2 = [None for _ in range(fnn.layerCount)]
        super(SMORMS3, self).__init__(fnn, lamb, options)
    
    def apply_update_rule(self, params, grad, l):
        if(self.mem[l] is None):
            self.mem[l] = np.ones(params.shape)
            self.g[l] = np.zeros(params.shape)
            self.g2[l] = np.zeros(params.shape)
        r = 1/(self.mem[l]+1)
        self.g[l] = (1-r)*self.g[l]+r*grad
        self.g2[l] = (1-r)*self.g2[l]+r*grad**2
        self.mem[l] *= (1 - self.g[l]*self.g[l]/(self.g2[l]+1e-16))
        self.mem[l] += 1
        return params - grad*np.minimum(self.learningRate,
            self.g[l]*self.g[l]/(self.g2[l]+1e-16))/(np.sqrt(self.g2[l])+1e-16)