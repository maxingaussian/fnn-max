import numpy as np
from .Layer import Layer as Layer


class LinearLayer(Layer):
    
    """ Layer using Linear function """
    
    def __init__(self, size=0, nextLayer=None, edges=None):
        super(LinearLayer, self).__init__(size, nextLayer, edges)
        if(nextLayer is None or edges is None):
            return
        b = np.sqrt(6./(self.size+self.nextLayer.size))/2
        W = np.random.randn(self.size+1, self.nextLayer.size)*b
        self.params = self.edges*W
    
    def reset(self, size=0, nextLayer=None, edges=None):
        self.__init__(size, nextLayer, edges)
    
    def set_edges(self, edges):
        self.__init__(self.size, self.nextLayer, edges)
    
    def transfer_function(self, X):
        self.Z = np.dot(X, self.params).copy()
        self.A = self.Z.copy()
    
    def transfer_function_jacobian(self, Z):
        return np.ones(Z.shape)
    
    def dZdp(self):
        return np.dot(self.X.T, self.delta)