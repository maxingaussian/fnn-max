import numpy as np
from .Layer import Layer as Layer


class SoftMaxLayer(Layer):
    
    """ Layer for Softmax function """
    
    func = ""
    
    def __init__(self, size=0, nextLayer=None, edges=None, func="rand"):
        if(func == "rand"):
            funcs = ["sig", "abs", "tanh", "gauss", "sinu", "sinc", "bent", "soft"]
            self.func = funcs[np.random.randint(len(funcs))]
            if(np.random.randint(len(funcs))%2):
                self.func += "a"
            print("Randomly Picked Function:", self.func)
        else:
            self.func = func
        super(SoftMaxLayer, self).__init__(size, nextLayer, edges)
        if(nextLayer is None or edges is None):
            return
        b = np.sqrt(6./(self.size+self.nextLayer.size))/2
        W = np.random.randn(self.size+1, self.nextLayer.size)*b
        self.params = self.edges*W
    
    def reset(self, size=0, nextLayer=None, edges=None):
        self.__init__(size, nextLayer, edges, self.func)
    
    def set_edges(self, edges):
        self.__init__(self.size, self.nextLayer, edges)
    
    def transfer_function(self, X):
        self.Z = np.dot(X, self.params).copy()
        self.A = self.sofmax_func(self.Z).copy()
    
    def transfer_function_jacobian(self, Z):
        return self.sofmax_func(Z, deriv=True)
    
    def dZdp(self):
        return np.dot(self.X.T, self.delta)
    
    def sofmax_func(self, Z, deriv=False):
        res = np.zeros(Z.shape)
        if("sig" in self.func):
            if(deriv):
                res += self.sofmax_func(Z)*(1-self.sofmax_func(Z))
            res += 1./(1+np.exp(-Z))
        elif("abs" in self.func):
            if(deriv):
                res += 1./np.square(1+np.abs(Z))
            res += Z/(1+np.abs(Z))
        elif("tanh" in self.func):
            if(deriv):
                res += 1.14393*(1/np.cosh(2*Z/3.))**2
            res += 1.7159*np.tanh(0.66666667*Z)
        elif("gauss" in self.func):
            if(deriv):
                res += -2*Z*np.exp(-Z**2)
            res += np.exp(-Z**2)
        elif("sinu" in self.func):
            if(deriv):
                res += np.cos(Z)
            res += np.sin(Z)
        elif("sinc" in self.func):
            nonZeros = (Z!=0)
            if(deriv):
                res += nonZeros*(np.cos(Z)/Z-np.sin(Z)/(Z**2))
            res = np.ones(Z.shape)
            res += nonZeros*(np.sin(Z)/Z)
        elif("bent" in self.func):
            if(deriv):
                res += Z/np.sqrt(4*((Z**2)+1))+1
            res += (np.sqrt((Z**2)+1)-1)/2.+Z
        elif("soft" in self.func):
            if(deriv):
                res += 1./(1+np.exp(-Z))
            res += np.log(1+np.exp(Z))
        if(self.func[-1] == 'a'):
            if(deriv):
                res += 1e-2
            res += 1e-2*Z
        return res
        