import abc
import numpy as np


class Layer(object):
    
    """ Basic Layer for Feed-forward Neural Network """
    
    # Class Members
    size = 0
    nextLayer = None
    edges = None
    X, Z, A, delta = None, None, None, None
    params = None
    grad = None
    
    # Class Methods
    def __init__(self, size=0, nextLayer=None, edges=None):
        self.size = size
        if(nextLayer is None or edges is None):
            return
        self.nextLayer = nextLayer
        self.edges = np.vstack((edges, np.ones((1, edges.shape[1]), dtype=bool)))
    
    def forward(self, X):
        self.X = np.hstack((X, np.ones((X.shape[0], 1))))
        self.transfer_function(self.X)
    
    def backward(self, delta, lamb=1e-3):
        dAdZ = self.transfer_function_jacobian(self.Z)
        if(self.nextLayer.nextLayer is None):
            self.delta = np.multiply((self.A-delta), dAdZ)
        else:
            self.delta = np.multiply(np.dot(delta, self.nextLayer.params.T)[:, :-1], dAdZ)
        self.grad = self.dZdp()[:, :self.params.shape[1]]/self.X.shape[0]+lamb*(self.params)
    
    __metaclass__ = abc.ABCMeta
    
    @abc.abstractmethod
    def transfer_function(self, X):
        pass
    
    @abc.abstractmethod
    def transfer_function_jacobian(self, Z):
        pass
    
    @abc.abstractmethod
    def dZdp(self):
        pass
    
                