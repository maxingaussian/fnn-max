import math
import numpy as np
from Layers import *
from Trainers import *

class FNN(object):
    
    """ Feed-forward Neural Network """
    
    layerCount = 0
    shape = None
    layers = None
    X_train, y_train = None, None
    X_valid, y_valid = None, None
    X_scaler, y_scaler = None, None
    
    def __init__(self, shape):
        self.layerCount = len(shape)-1
        self.shape = list(shape)
        self.layers = [Layer(self.shape[-1])]
        for l in range(self.layerCount):
            m, n = self.shape[-2-l], self.shape[-1-l]
            edges = np.ones((m, n), dtype=bool)
            self.layers.insert(0, LinearLayer(m, self.layers[0], edges))
    
    def set_layer(self, l, layer, edges=None):
        self.layers[l] = layer
        m, n = self.shape[l], self.shape[l+1]
        if(edges is None):
            edges = np.ones((m, n), dtype=bool)
        self.layers[l].reset(m, self.layers[l+1], edges)
        if(l > 0):
            self.layers[l-1].nextLayer = self.layers[l]
    
    def transform(self, X=None, y=None):
        _X, _y = None, None
        if(X is not None):
            _X = (X-self.X_scaler[0])/(self.X_scaler[1]*2+1)
        if(y is not None):
            _y = (y-self.y_scaler[0])/(self.y_scaler[1]*2+1)
        return _X, _y
    
    def inverse_transform(self, X=None, y=None):
        _X, _y = None, None
        if(X is not None):
            _X = X*(self.X_scaler[1]*2+1)+self.X_scaler[0]
        if(y is not None):
            _y = y*(self.y_scaler[1]*2+1)+self.y_scaler[0]
        return _X, _y
    
    def fit(self, X_train, y_train, X_valid=None, y_valid=None, batchSize=-1, trainer=None):
        if(trainer is None):
            trainer = SMORMS3(self, 0.0001)
        if(batchSize == -1):
            batchSize = int(np.sqrt(X_train.shape[0]))
        self.X_scaler = (np.mean(X_train, axis=0), np.std(X_train, axis=0))
        self.y_scaler = (np.mean(y_train, axis=0), np.std(y_train, axis=0))
        self.X_train, self.y_train = self.transform(X_train, y_train)
        if(X_valid is not None):
            self.X_valid, self.y_valid = self.transform(X_valid, y_valid)
        trainer.train(self.X_train, self.y_train, self.X_valid, self.y_valid, batchSize)
        
    def predict(self, X_test):
        X, _ = self.transform(X_test)
        fnn_output = self.feedforward(X)
        _, y = self.inverse_transform(None, fnn_output)
        return y
    
    def feedforward(self, X):
        self.layers[0].forward(X)
        for l in range(1, self.layerCount):
            self.layers[l].forward(self.layers[l-1].A)
        return self.layers[-2].A
    
    def backpropagate(self, X, y, lamb=1e-3):
        self.feedforward(X)
        self.layers[-2].backward(y, lamb)
        for l in range(self.layerCount-2, -1, -1):
            self.layers[l].backward(self.layers[l+1].delta, lamb)
            
    def search_topology(self, X_train, y_train, X_valid=None, y_valid=None, edgesLimit=None, trials=5):
        if(edgesLimit is None):
            edgesLimit = [int(self.shape[l]*self.shape[l+1]/1.8) for l in range(self.layerCount)]
        if(X_valid is None):
            N = X_train.shape[0]
            valid_size = int(0.4*N)
            X_valid, y_valid = X_train[:valid_size].copy(), y_train[:valid_size].copy()
            X_train, y_train = X_train[valid_size+1:].copy(), y_train[valid_size+1:].copy()
        for l in range(self.layerCount):
            min_err = 1e10
            best_edges = None
            for t in range(int(trials*edgesLimit[l])):
                edges = np.zeros((self.shape[l], self.shape[l+1]), dtype=bool)
                edges_num = np.random.choice(edges.size, edgesLimit[l], replace=False)
                for ed in edges_num:
                    edges[ed/edges.shape[1], ed%edges.shape[1]] = True
                self.set_layer(l, self.layers[l], edges)
                self.fit(X_train, y_train, X_valid, y_valid, batchSize=20, trainer=SMORMS3(fnn, 0.01, (500, 30)))
                y_pred = self.feedforward(X_valid)
                err = np.double(np.sum((y_valid-y_pred)**2))*0.5/X_valid.shape[0]
                print(t, ":", min_err)
                if(err < min_err):
                    min_err = err
                    best_edges = edges.copy()
            self.set_layer(l, self.layers[l], best_edges)
    
    def plot_topology(self):
        import pylab
        import networkx as nx
        pylab.figure()
        G = nx.DiGraph()
        for i in range(self.shape[0]):
            G.add_node("X"+str(self.shape[0]-i),
                pos=(0, i-self.shape[0]/2+(self.shape[0]%2)*0.5))
        for l in range(self.layerCount):
            if(l == self.layerCount-1):
                for i in range(self.shape[l+1]):
                    G.add_node("Y"+str(self.shape[l+1]-i),
                        pos=(l+1, i-self.shape[l+1]/2+(self.shape[l+1]%2)*0.5))
                break
            for i in range(self.shape[l+1]):
                G.add_node("L"+str(l+1)+str(self.shape[l+1]-i),
                    pos=(l+1, i-self.shape[l+1]/2+(self.shape[l+1]%2)*0.5))
        edgeList = []
        for l in range(self.layerCount):
            if(l == 0):
                for i in range(self.shape[l]):
                    for j in range(self.shape[l+1]):
                        if(self.layers[l].edges[i, j]):
                            edgeList.append(["X"+str(i+1), "L"+str(l+1)+str(j+1)])
            elif(l == self.layerCount-1):
                for i in range(self.shape[l]):
                    for j in range(self.shape[l+1]):
                        if(self.layers[l].edges[i, j]):
                            edgeList.append(["L"+str(l)+str(i+1), "Y"+str(j+1)])
            else:
                for i in range(self.shape[l]):
                    for j in range(self.shape[l+1]):
                        if(self.layers[l].edges[i, j]):
                            edgeList.append(["L"+str(l)+str(i+1), "L"+str(l+1)+str(j+1)])
        G.add_edges_from(edgeList)
        pos = nx.get_node_attributes(G, 'pos')
        nx.draw(G, pos, node_size=1300, with_labels=True, node_color='g')
        pylab.show()    

    
    
    
    
    
    