import os, sys
sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(os.path.realpath('__file__')), os.pardir)))
from Models import FNN
from Layers import *
from Trainers import *
import numpy as np
from sklearn.metrics import accuracy_score

def load_mnist_data(pos_class_lbl=1, neg_class_lbl=0):
    print("Loading MNIST Data......")
    import pickle, gzip
    f = gzip.open('../Datasets/MNIST/mnist.pkl.gz', 'rb')
    u = pickle._Unpickler(f)
    u.encoding = 'latin1'
    train_set, valid_set, test_set = u.load()
    f.close()
    X_train, y_target = train_set
    X_valid, y_target2 = valid_set
    X_test, y_target3 = test_set
    y_class = set(y_target.tolist())
    y_train = np.ones((X_train.shape[0], len(y_class))) * neg_class_lbl
    for i in range(y_train.shape[0]):
        y_train[i, y_target[i]] = pos_class_lbl
    y_valid = np.ones((X_valid.shape[0], len(y_class))) * neg_class_lbl
    for i in range(y_valid.shape[0]):
        y_valid[i, y_target2[i]] = pos_class_lbl
    y_test = np.ones((X_test.shape[0], len(y_class))) * neg_class_lbl
    for i in range(y_test.shape[0]):
        y_test[i, y_target3[i]] = pos_class_lbl
    print("Loaded MNIST Data!")
    return X_train, y_train, X_valid, y_valid, X_test, y_test
    
def load_iris_data(pos_class_lbl=1, neg_class_lbl=0, proportion=0.4):
    print("Loading IRIS Data......")
    import sklearn.datasets as datasets
    from sklearn import cross_validation
    iris = datasets.load_iris()
    X = iris.data
    y = iris.target
    X_train, X_test, y_target, y_target2 = \
        cross_validation.train_test_split(X, y, test_size=proportion)
    y_class = set(y_target.tolist())
    y_train = np.ones((X_train.shape[0], len(y_class))) * neg_class_lbl
    for i in range(y_train.shape[0]):
        y_train[i, y_target[i]] = pos_class_lbl
    y_test = np.ones((X_test.shape[0], len(y_class))) * neg_class_lbl
    for i in range(y_test.shape[0]):
        y_test[i, y_target2[i]] = pos_class_lbl
    print("Loaded IRIS Data!")
    return X_train, y_train, None, None, X_test, y_test

X_train, y_train, X_valid, y_valid, X_test, y_test =\
    load_mnist_data(pos_class_lbl=1, neg_class_lbl=0)
shape = [X_train.shape[1]]
hiddenLayers = 2
for i in range(hiddenLayers):
    shape.append(y_train.shape[1]+int(X_train.shape[1]**(1./(i+2))))
shape.append(y_train.shape[1])
print("Topology:", shape)
fnn = FNN(shape)
fnn.set_layer(0, SoftMaxLayer(func="abs"))
fnn.set_layer(1, SoftMaxLayer(func="abs"))
fnn.set_layer(2, SoftMaxLayer(func="abs"))
fnn.set_layer(fnn.layerCount-1, SoftMaxLayer(func="abs"))
trainer = SMORMS3(fnn, 0.0005, (np.Infinity, 100, 1e-5))
fnn.fit(X_train, y_train, trainer=trainer)
target = np.apply_along_axis(lambda a:np.argmax(a), 1, y_test)
prediction = np.apply_along_axis(lambda a:np.argmax(a), 1, fnn.predict(X_test))
print("Test Accuracy =", accuracy_score(target, prediction))
target = np.apply_along_axis(lambda a:np.argmax(a), 1, y_train)
prediction = np.apply_along_axis(lambda a:np.argmax(a), 1, fnn.predict(X_train))
print("Train Accuracy =", accuracy_score(target, prediction))