import os, sys
sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(os.path.realpath('__file__')), os.pardir)))
from Models import FNN
from Layers import *
from Trainers import *
import numpy as np
from sklearn.metrics import mean_squared_error

def load_1d_function_data(true_func):
    N = 500
    X_train = np.vstack((np.random.rand(N/2, 1)*N/25,
                   N/25+N/50+np.random.rand(N/2, 1)*N/25))
    noise = 0.1
    y_train = true_func(X_train)+np.random.randn(X_train.shape[0], 1)*noise
    X_test = np.random.rand(N/2, 1)*N/50+N/25
    y_test = true_func(X_test)+np.random.randn(X_test.shape[0], 1)*noise
    return X_train, y_train, None, None, X_test, y_test

def load_boston_data(proportion=0.1):
    from sklearn import datasets
    from sklearn.utils import shuffle
    from sklearn import cross_validation
    boston = datasets.load_boston()
    X, y = shuffle(boston.data, boston.target,
                   random_state=np.random.RandomState(np.random.randint(1001)))
    y = y[:, None]
    X = X.astype(np.float64)
    X_train, X_test, y_train, y_test = \
        cross_validation.train_test_split(X, y, test_size=proportion)
    return X_train, y_train, None, None, X_test, y_test

def plot_1d_test_function(X_train, y_train, predict_func, true_func=None):
    pts = 500
    _X, _y = X_train.copy(), y_train.copy()
    grows = np.ceil(np.sqrt(_X.shape[1]))
    import matplotlib.pyplot as plt
    plt.figure()
    for d in range(_X.shape[1]):
        ax = plt.subplot(grows, grows, d + 1)
        xrng = _X[:, d].max() - _X[:, d].min()
        Xs = np.tile(np.linspace(_X[:, d].min() - 0.3*xrng,
            _X[:, d].max() + 0.3*xrng, pts)[:, None], _X.shape[1])
        mu = predict_func(Xs)
        mu = mu.ravel()
        ax.plot(Xs[:, d], mu[:], 'black')
        if(true_func is not None):
            ax.plot(Xs[:, d], true_func(Xs)[:], 'r--')
        ax.errorbar(_X[:, d], _y.ravel(), fmt='r.', markersize=10)
        yrng = _y.max() - _y.min()
        plt.ylim(_y.min() - 0.5*yrng, _y.max() + 0.5*yrng)
        plt.xlim(Xs[:, d].min(), Xs[:, d].max())
    del _X
    plt.show()

X_train, y_train, X_valid, y_valid, X_test, y_test = \
    load_boston_data()
shape = [X_train.shape[1]]
for i in range(3):
    shape.append((i+1)*X_train.shape[1])
shape.append(y_train.shape[1])
fnn = FNN(shape)
for i in range(1, fnn.layerCount-1):
    fnn.set_layer(i, SoftMaxLayer())
trainer = SMORMS3(fnn, 0.0002, (np.Infinity, 100, 1e-6), 0.003)
fnn.fit(X_train, y_train, batchSize=5, trainer=trainer)
fnn.predict(X_test)
plot_1d_test_function(X_train, y_train, fnn.predict, true_func=None)
print("Test MSE =", mean_squared_error(fnn.predict(X_test), y_test))
print("Train MSE =", mean_squared_error(fnn.predict(X_train), y_train))